# README

In November 2022, I wrote [Automate HTTP Testing with hurl: Generate HTML and JUnit reports via GitLab CI](https://brie.dev/2022-http-testing-hurl/). These are the public `.hurl` files that I have been working on since then. 
